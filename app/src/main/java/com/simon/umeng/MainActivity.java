package com.simon.umeng;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.umeng.analytics.MobclickAgent;
import com.umeng.message.PushAgent;

public class MainActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        // 统计应用启动数据，在应用的BaseActivity的onCreate方法中添加
        PushAgent.getInstance(this).onAppStart();
        MobclickAgent.onProfileSignIn("18774979257");
        MobclickAgent.onProfileSignIn("LSC", "18774979257");
        MobclickAgent.onEvent(context, "click_main");

        findViewById(R.id.b1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ShareActivity.class));
            }
        });

        findViewById(R.id.b2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, PushActivity.class));
            }
        });

        findViewById(R.id.b3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, AnalyticsActivity.class));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        // 程序退出时，用于保存统计数据的API（调用kill或者exit之类的方法杀死进程前调用）
        MobclickAgent.onKillProcess(this);
        super.onDestroy();
    }
}
