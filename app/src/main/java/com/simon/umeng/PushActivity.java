package com.simon.umeng;

import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.umeng.message.IUmengCallback;

/**
 * 友盟推送
 */
public class PushActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push);
        context = this;

        MobclickAgent.onEvent(context, "click_push");

        findViewById(R.id.b1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enable();
            }
        });

        findViewById(R.id.b2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disable();
            }
        });
    }

    // 开启推送（在Activity内调用）
    private void enable() {
        BaseApplication.getInstance().getmPushAgent().enable(new IUmengCallback() {

            @Override
            public void onSuccess() {
                Toast.makeText(context, "开启推送成功", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(String s, String s1) {
                Toast.makeText(context, s + " / " + s1, Toast.LENGTH_LONG).show();
            }

        });
    }

    // 关闭推送（在Activity内调用）
    private void disable() {
        BaseApplication.getInstance().getmPushAgent().disable(new IUmengCallback() {

            @Override
            public void onSuccess() {
                Toast.makeText(context, "关闭推送成功", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(String s, String s1) {
                Toast.makeText(context, s + " / " + s1, Toast.LENGTH_LONG).show();
            }

        });
    }
}
