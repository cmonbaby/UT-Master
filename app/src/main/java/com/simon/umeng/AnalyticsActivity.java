package com.simon.umeng;

import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;
import java.util.Map;

/**
 * 友盟统计
 */
public class AnalyticsActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analytics);
        context = this;

        // 每个应用至多添加500个自定义事件，每个event 的 key不能超过10个
        // 每个key的取值不能超过1000个。如需要统计支付金额、使用时长等数值型的连续变量，请使用计算事件
        findViewById(R.id.b1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 计数事件
                MobclickAgent.onEvent(context, "click_analytics"); // 自定义事件id
                MobclickAgent.onEvent(context, "commit", "order"); // 自定义事件id，事件的标签属性

                // 统计点击行为各属性被触发的次数
                HashMap<String, String> map = new HashMap<>();
                map.put("phone", "18612345678");
                map.put("name", "hello world");
                MobclickAgent.onEvent(context, "id_user_info", map);
            }
        });

        findViewById(R.id.b2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 计算事件（统计一次音乐播放，包括音乐类型，作者和播放时长，可以在音乐播放结束后）
                int duration = 12000; //开发者需要自己计算音乐播放时长
                Map<String, String> map_value = new HashMap<>();
                map_value.put("type", "popular");
                map_value.put("artist", "JJLin");
                MobclickAgent.onEventValue(context, "music", map_value, duration);
            }
        });

        findViewById(R.id.b3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 登出（账号登出时需调用此接口，调用之后不再发送账号相关内容）
                MobclickAgent.onProfileSignOff();
            }
        });

        // 将默认Session间隔时长30秒改为40秒。
        MobclickAgent.setSessionContinueMillis(1000 * 40);

        // 当用户使用自有账号登录时（用户账号ID，长度小于64字节）
        MobclickAgent.onProfileSignIn("18774979257");
        // 当用户使用第三方账号（如新浪微博）登录时
        // 支持自定义，不能以下划线”_”开头，使用大写字母和数字标识，长度小于32 字节; 如果是上市公司，建议使用股票代码）
        MobclickAgent.onProfileSignIn("LSC", "18774979257");

        // 错误统计（捕获程序崩溃日志，并在程序下次启动时发送到服务器）
        MobclickAgent.setCatchUncaughtExceptions(false);
    }

    // Session启动、App使用时长等基础数据统计。在App中每个Activity的onResume()和onPause()方法中调用
    // 如果页面是使用FragmentActivity + Fragment实现的，Activity统计市场，Fragment中onPageXX()
    @Override
    public void onResume() {
        super.onResume();
//        MobclickAgent.onPageStart("SplashScreen"); // 手动统计页面("SplashScreen"为页面名称，可自定义)
        MobclickAgent.onResume(this); // 统计时长
    }

    @Override
    public void onPause() {
        super.onPause();
        // 手动统计页面("SplashScreen"为页面名称，可自定义)
        // 必须保证 onPageEnd 在 onPause 之前调用，因为SDK会在 onPause 中保存onPageEnd统计到的页面数据。
//        MobclickAgent.onPageEnd("SplashScreen");
        MobclickAgent.onPause(this); // 统计时长
    }

}
