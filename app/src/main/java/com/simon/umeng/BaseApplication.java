package com.simon.umeng;

import android.app.Application;
import android.util.Log;

import com.simon.umeng.push.UmengNotificationService;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.MsgConstant;
import com.umeng.message.PushAgent;
import com.umeng.socialize.PlatformConfig;

import org.android.agoo.huawei.HuaWeiRegister;
import org.android.agoo.mezu.MeizuRegister;
import org.android.agoo.xiaomi.MiPushRegistar;

public class BaseApplication extends Application {

    public static BaseApplication app;
    protected PushAgent mPushAgent;

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;
        /**
         * 设置组件化的Log开关
         * 参数: boolean 默认为false，如需查看LOG设置为true
         */
        UMConfigure.setLogEnabled(true);

        /**
         * 初始化common库
         * 参数1:上下文，不能为空
         * 参数2:【友盟+】 AppKey，非必须参数，如果Manifest文件中已配置AppKey，该参数可以传空
         * 参数3:【友盟+】 Channel，非必须参数，如果Manifest文件中已配置Channel，该参数可以传空
         * 参数4:设备类型，UMConfigure.DEVICE_TYPE_PHONE为手机、UMConfigure.DEVICE_TYPE_BOX为盒子，默认为手机
         * 参数5:Push推送业务的secret
         */
        UMConfigure.init(this, "5b471ec5a40fa312c1000037"
                , "link_car", UMConfigure.DEVICE_TYPE_PHONE, "a2c5b02c9d259d93805e40390ab0b63b");

        initApp();
        initPush();
    }

    // 初始化统计
    private void initApp() {
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL); // 普通统计场景
        MobclickAgent.setSecret(this, "企业认证后的secretKey"); // 设置secretkey
    }

    // 初始化推送
    private void initPush() {
        mPushAgent = PushAgent.getInstance(this);
        //注册推送服务，每次调用register方法都会回调该接口
        mPushAgent.register(new IUmengRegisterCallback() {

            @Override
            public void onSuccess(String deviceToken) {
                Log.e("deviceToken >>> ", deviceToken);
            }

            @Override
            public void onFailure(String s, String s1) {
                Log.e("deviceToken >>> ", s + " / " + s1);
            }
        });

        mPushAgent.setPushIntentServiceClass(UmengNotificationService.class);
        // 服务端控制
        mPushAgent.setNotificationPlaySound(MsgConstant.NOTIFICATION_PLAY_SERVER); // 声音
        mPushAgent.setNotificationPlayLights(MsgConstant.NOTIFICATION_PLAY_SERVER); // 呼吸灯
        mPushAgent.setNotificationPlayVibrate(MsgConstant.NOTIFICATION_PLAY_SERVER); // 振动

        // 小米通道
        MiPushRegistar.register(this, "2882303761517836907", "5801783656907");
        // 华为通道
        HuaWeiRegister.register(this);
        // 魅族通道
        MeizuRegister.register(this, "114603", "dfa96c3f785c46dcbacb6ac289e15619");
    }

    public static BaseApplication getInstance() {
        return app;
    }

    public PushAgent getmPushAgent() {
        return mPushAgent;
    }

    // 分享平台静态块初始化：AppID + APP Key（AppSecret）
    static {
        PlatformConfig.setWeixin("wx1adae26662887b1e", "65a8ac4d01aa74f87a89e5cfec304632");
        PlatformConfig.setQQZone("101485926", "5da885cd011f08358d455b5afa6b754f");
    }

}
